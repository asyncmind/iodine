#!/usr/bin/env python
"""Iodine - A salt api client using IPython shell

Usage:
    iodine [options] [<user@master:port>]

Options:

    -n --noupdatecheck    Dont check for updates

"""
from __future__ import print_function
import os
import logging
from docopt import docopt
from iodine.magic import SaltMagics
import iodine.update
from getpass import getpass, getuser

# http://stackoverflow.com/questions/954834/how-do-i-use-raw-input-in-python-3-1
# Fix Python 2.x.
try:
    input = raw_input
except NameError:
    pass



def getopts():
    args = docopt(__doc__)
    if not args.get('--noupdatecheck'):
        iodine.update.update()
    uri = args['<user@master:port>']
    user, master, port = None, None, None
    if uri:
        if '@' in uri:
            user, master = uri.split('@')
        else:
            master = uri
        if ':' in master:
            master, port = master.split(':')
    default_user = getuser()
    default_master = 'localhost'
    return dict(
        username=(
            user or
            input("Username (%s):" % default_user) or
            default_user
        ),
        password=getpass('Password: '),
        master=(
            master or
            input("Master (%s):" % default_master) or
            default_master
        ),
        port=int(port) if port else 8001,
    )


def main():
    # http://ipython.org/ipython-doc/dev/interactive/reference.html#defining-your-own-magics
    logging.basicConfig(filename='/tmp/iodine.log', level=logging.DEBUG)
    from IPython.terminal.embed import InteractiveShellEmbed
    ipshell = InteractiveShellEmbed()
    magics = SaltMagics(ipshell, **getopts())
    ipshell.register_magics(magics)
    ipshell.set_hook(
        'complete_command', magics._salt_completer, str_key='%salt')
    ipshell()
    magics.client.logout()

if __name__ == '__main__':
    main()
