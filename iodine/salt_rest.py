from hammock import Hammock
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


# https://docs.saltstack.com/en/latest/topics/netapi/index.html


class RestClient():
    def __init__(self, opts):
        self.master = opts['master']
        self.port = opts.get('port', 80)
        self.username = opts['username']
        self.password = opts['password']
        self.eauth = opts.get('eauth', 'pam')
        self.session = Hammock("http://%(master)s:%(port)s" % opts)
        self._auth = dict(
            username=self.username,
            password=self.password,
            eauth=self.eauth,
        )

    def login(self):
        resp = self.session.login.POST(data=self._auth)
        assert resp.status_code == 200, resp.text

    def logout(self):
        resp = self.session.logout.POST()
        assert resp.status_code == 200, resp.text

    def _req(self, endpoint,  *args, **kwargs):
        count = 1
        while count:
            resp = endpoint(*args, **kwargs)
            if resp.status_code == 401:
                self.login()
            elif resp.status_code in [200, 202]:
                return resp.json()
            else:
                raise Exception(resp.text)
            count -= 1

    def run(self, tgt=None, fun=None, arg=None,
            client='local', expr_form='glob',
            **kwargs):
        data = dict(
            tgt=tgt, fun=fun, client=client, arg=arg,
            expr_form=expr_form,
            )
        log.debug("run: %s", data)
        #data = {k:v for k,v in data.items() if v}
        data.update(self._auth)
        return self._req(self.session.run.POST, data=data)

    def minions_run(self, tgt=None, fun=None, arg=None,
                    client='local', expr_form='glob', 
                    headers=None,
                    **kwargs):
        data = dict(
            tgt=tgt, fun=fun, client=client, arg=arg,
            expr_form=expr_form,
            )
        data.update(self._auth)
        return self._req(self.session.minions.POST, data=data)

    def minions(self):
        return self._req(self.session.minions.GET)

    def jobs(self, jid=None):
        data = dict(jid=jid)
        data.update(self._auth)
        if jid:
            return self._req(self.session.jobs.GET, jid)
        else:
            return self._req(self.session.jobs.GET)
